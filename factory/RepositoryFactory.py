from interface import Interface
from interface import implements
from repository.jsonBasedRepository import ContactJsonRepository, PageJsonRepository, \
    EssentialAssetsJsonRepository, ProductPropertiesJsonRepository, CssDataJsonRepository


class RepositoryFactory(Interface):

    def create_contact_repository(self):
        pass

    def create_assets_repository(self):
        pass

    def create_page_repository(self):
        pass

    def create_properties_repository(self):
        return PageJsonRepository()

    def create_css_repository(self):
        pass


class JsonBasedRespositoryFactory(implements(RepositoryFactory)):

        def create_contact_repository(self):
            return ContactJsonRepository()

        def create_assets_repository(self):
            return EssentialAssetsJsonRepository()

        def create_page_repository(self):
            return PageJsonRepository()

        def create_properties_repository(self):
            return ProductPropertiesJsonRepository()

        def create_css_repository(self):
            return CssDataJsonRepository()
