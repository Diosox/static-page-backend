from factory.RepositoryFactory import RepositoryFactory, JsonBasedRespositoryFactory
from service.ServicesImpl import PageServiceImpl, ContactServiceImpl, \
    EssentialAssetsServiceImpl, ProductPropertiesServiceImpl, CssDataServiceImpl


class ServiceFactory():

    def __init__(self, factory: RepositoryFactory=JsonBasedRespositoryFactory()):
        self.factory = factory

    def get_contact_service(self):
        return ContactServiceImpl(self.factory.create_contact_repository())

    def get_page_service(self):
        return PageServiceImpl(self.factory.create_page_repository())

    def get_assets_service(self):
        return EssentialAssetsServiceImpl(self.factory.create_assets_repository())

    def get_properties_service(self):
        return ProductPropertiesServiceImpl(self.factory.create_properties_repository())

    def get_css_service(self):
        return CssDataServiceImpl(self.factory.create_css_repository())
