#!/usr/bin/python3

import json
import os
import sys
from gevent.pywsgi import WSGIServer
from app import app

ROOT = os.path.dirname(os.path.abspath(__file__))


def get_port():
    with open(os.path.join(ROOT, "properties.json"), "r") as f:
        return json.loads(f.read())["ports"]["admin"]


def run_server():
    http_server = WSGIServer(('', get_port()), app)
    http_server.serve_forever()


if __name__ == "__main__":
    if sys.platform == "win32":
        run_server()
    else:
        import daemon
        from daemon.pidfile import PIDLockFile
        pidfile = PIDLockFile(os.path.join(ROOT, "instance.pid"))
        out = open(os.path.join(ROOT, "flask.log"), "a")
        with daemon.DaemonContext(pidfile=pidfile, stdout=out, stderr=out):
            run_server()
