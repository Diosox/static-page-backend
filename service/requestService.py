from flask import request, jsonify
from factory.ServiceFactory import ServiceFactory
from service.services import *
from . imageService import save_image
from .imageService import  upload_image, upload_images

service_factory = ServiceFactory()
page_service: PageService = service_factory.get_page_service()
contact_service: ContactService = service_factory.get_contact_service()
assets_service:EssentialAssetsService = service_factory.get_assets_service()
css_service:CssDataService = service_factory.get_css_service()


def update_page():
    description = request.json['about_page']['description']
    about_us =request.json['about_page']['about_us']
    slogan = request.json['about_page']['slogan']

    return page_service.update_page_data(description,about_us, slogan)

def update_contact():
    address = request.json['contact']['address']
    phone = request.json['contact']['phone']
    email = request.json['contact']['email']
    google_maps_url = request.json['contact']['google_map_url']
    fb_url = request.json['contact']['fb_url']
    instagram_url = request.json['contact']['instagram_url']

    return contact_service.update_contact_data(address, phone,
                                        email, google_maps_url=google_maps_url, fb_url=fb_url, instagram_url=instagram_url)

def update_assets():
    logoUrl = request.json['essential_assets']['logoUrl']
    bannerUrl = request.json['essential_assets']['bannerUrl']

    return assets_service.update_assets_data(logoUrl, bannerUrl)

def update_styles():
    theme = request.json['styles']
    return css_service.update_css_data(theme)

def upload_files():
    images_url = upload_image(request)
    return jsonify(images_url)
