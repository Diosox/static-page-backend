from interface import implements
from model.interface.serializable import Serializable

class Page(implements(Serializable)):

    def __init__(self, description, about_us, slogan):
        self.description = description
        self.about_us = about_us
        self.slogan = slogan
        self.normalize_parameter()

    def normalize_parameter(self):
        if  type(self.description) is list:
            self.description = ''.join(self.description)
        if type(self.about_us) is list:
            self.about_us = ''.join(self.slogan)

    def serialize(self):
        return {
            'page' : {
            'description' : self.description,
                'about_us' : self.about_us,
                'slogan' : self.slogan
            }
        }