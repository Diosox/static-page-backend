from interface import implements
from model.interface.serializable import Serializable

class ProductProperties(implements(Serializable)):

    def __init__(self, product_name, subdomain, maintenance_mode):
        self.product_name = product_name
        self.subdomain = subdomain
        self.maintenance_mode = maintenance_mode

    def serialize(self):
        return {
            'product_name' : self.product_name,
            'subdomain': self.subdomain,
            'maintenance_mode': self.maintenance_mode
        }
