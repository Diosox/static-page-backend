from interface import Interface

class ContactRepository(Interface):

    def get_contact(self):
        pass

    def update_contact(self, contact_data):
        pass

class EssentialAssetsRepository(Interface):

    def get_assets(self):
        pass

    def update_assets(self, assets_data):
        pass

class PageRepository(Interface):

    def get_page(self):
        pass

    def update_page(self, page_data):
        pass

class ProductPropertiesRepository(Interface):

    def get_properties(self):
        pass

class CssDataRepository(Interface):

    def get_css_path(self):
        pass

    def update_css_data(self, data):
        pass

    def get_theme(self):
        pass
