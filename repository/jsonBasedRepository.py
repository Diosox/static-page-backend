import os
from repository.repository import *
from service.fileManagementService import open_json_data, get_file_path
from model.Page import Page
from model.Contact import Contact
from model.Essential_Assets import EssentialAssets
from model.Product_Properties import ProductProperties
from service.fileManagementService import write_json_files
from settings.constants import JSON_DATA, CSS_DIR, PRODUCT_JSON_FILE
from interface.interface import implements

class PageJsonRepository(implements(PageRepository)):

    def get_page(self):
        data = open_json_data()
        page_data = Page(data['about_page']['description'],
                         data['about_page']['about_us'], data['about_page']['slogan'])
        return page_data


    def update_page(self, page_data):
        description = page_data.description
        about_us = page_data.about_us
        slogan = page_data.slogan
        data = open_json_data()
        description_as_list = description.split('\n')
        print(description_as_list)
        data['about_page']['description'] = description_as_list
        data['about_page']['about_us'] = about_us
        data['about_page']['slogan'] = slogan
        print(data)
        print(about_us)
        write_json_files(JSON_DATA, data)

class ContactJsonRepository(implements(ContactRepository)):

    def get_contact(self):
        raw_json_data = open_json_data()
        address = raw_json_data['contact']['address']
        google_map_url = raw_json_data['contact']['google_map_url']
        email = raw_json_data['contact']['email']
        phone = raw_json_data['contact']['phone']
        fb_url = raw_json_data['contact']['fb_url']
        instagram_url = raw_json_data['contact']['instagram_url']
        contact_data = Contact(address, phone, email,
                               google_map_url=google_map_url, fb_url=fb_url, instagram_url=instagram_url)
        return contact_data

    def update_contact(self, contact_data):
        raw_json = open_json_data()
        raw_json['contact']['address'] = contact_data.address
        raw_json['contact']['google_map_url'] = contact_data.url_dict.get('google_maps_url')
        raw_json['contact']['email'] = contact_data.email
        raw_json['contact']['phone'] = contact_data.phone
        raw_json['contact']['fb_url'] = contact_data.url_dict.get('fb_url')
        raw_json['contact']['instagram_url'] = contact_data.url_dict.get('instagram_url')
        write_json_files(JSON_DATA, raw_json)

class EssentialAssetsJsonRepository(implements(EssentialAssetsRepository)):

    def get_assets(self):
        raw_assets_json = open_json_data()['essential_assets']
        logo_url = raw_assets_json['logoUrl']
        banner_url = raw_assets_json['bannerUrl']
        assets_data = EssentialAssets(logo_url, banner_url)
        return assets_data

    def update_assets(self, assets_data):
        raw_assets_json = open_json_data()
        raw_assets_json['essential_assets']['logoUrl'] = assets_data.logo_url
        raw_assets_json['essential_assets']['bannerUrl'] = assets_data.banner_url
        write_json_files(JSON_DATA, raw_assets_json)

class ProductPropertiesJsonRepository(implements(ProductPropertiesRepository)):

    def get_properties(self):
        raw_properties_json = open_json_data(filename=PRODUCT_JSON_FILE)
        product_name = raw_properties_json['original_product_name']
        subdomain = raw_properties_json['product_subdomain']
        maintenance_mode = False # always turn off maintenance mode
        properties_data = ProductProperties(product_name, subdomain, maintenance_mode)
        return properties_data

class CssDataJsonRepository(implements(CssDataRepository)):


    def get_css_path(self):
        raw_json = open_json_data()
        css_file_name = 'styles_' + raw_json['styles'] + '.css'
        return get_file_path(os.path.join(CSS_DIR, css_file_name))

    def update_css_data(self, data):
        raw_json = open_json_data()
        raw_json['styles'] = data
        write_json_files(JSON_DATA, raw_json)

    def get_theme(self):
        raw_json = open_json_data()
        return {
            'styles' :raw_json['styles']
        }
